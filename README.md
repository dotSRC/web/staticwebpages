# dotsrc frontpage

Most things are rather vanilla so check out Hugo and Beautifulhugo docs :)


## Quick Start

1. `git submodule init`
2. `git submodule update`
3. `hugo serve`


## Pushing to Production
The content of the website can be changed by pushing changes to a git repository on `dotsrc.org`.
This is done as the `git` user over ssh, so you ssh key has to be appended to `authorized_keys` before you can push.

To make this easier, you can add the following upstream:
- `git remote add webserver git@dotsrc.org:/srv/git/staticpages.git`

**NB!** Before pushing to production check your changes by pushing to gitlab master and verify that the changes look as expected https://dotsrc.gitlab.io/web/staticwebpages.


This gives the following workflow:

1. Create change in new MR (Merge Request)
2. Wait for change to be merged into master
3. Verify change on https://dotsrc.gitlab.io/web/staticwebpages
4. Pull changes
5. Push changes to dotsrc.org (`git push webserver master`).

# Social Media

[LinkedIn](https://www.linkedin.com/company/dotsrc)

[Facebook](https://www.facebook.com/dotsrc)
