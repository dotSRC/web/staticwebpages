+++
title = "About dotsrc"
url = "/aboutus/hej"
+++

Dotsrc.org (formerly SunSITE.dk) is a not-for-profit organization providing a variety of services to the open source community. The primary services offered by us are the mirroring of popular open source software, irc and usenet access.

Dotsrc.org is a completely non-commercial project, powered by sponsored hardware, and driven by a group of volunteers.

We can be contacted via mail to staff@dotsrc.org (usually the preferred way) and via IRCNet, channel #dotsrc.

Our main sponsors are:

{{< columns >}}
<a href="sponsors/#danish-e-infrastructure-cooperation">
<img src="./img/sponsors/deic.svg" height="50" alt="Danish e-Infrastructure Cooperation logo"/>
</a>
{{< column >}}
<a href="sponsors/#dansk-unix-user-group">
<img src="./img/sponsors/dkuug.svg" height="40" alt="Dansk Unix User Group logo"/>
</a>
{{< endcolumns >}}

<a href="sponsors/#aalborg-university">
<img src="./img/sponsors/aau.svg" height="113" alt="Aalborg University logo"/>
</a>

{{< columns >}}
<!-- Sponsor spot reserved for you or your employer, send us an email :) -->
{{< column >}}
<a href="sponsors/#prosa">
<img src="./img/sponsors/prosa.png" height="40" alt="PROSA logo"/>
</a>

{{< endcolumns >}}
