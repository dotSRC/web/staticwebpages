+++
title = "About Us"
+++

[free/libre]: https://www.gnu.org/
[open source]: https://opensource.org/
[IRCNet]: https://www.ircnet.com/

Dotsrc.org (formerly SunSITE.dk) is a not-for-profit organization providing a variety of services to the [free/libre] and [open source] community.
The primary services offered by us are the mirroring of popular open source software, IRC and Usenet access.

Dotsrc.org is a completely non-commercial project, powered by sponsored hardware, and driven by a group of volunteers.

We can be contacted via mail to staff@dotsrc.org (usually the preferred way) and via [IRCNet], channel #dotsrc. 
