+++
title = "Equipment"
url = "aboutus/equipment/"
+++

dotsrc.org rely on donated equipment, and on this page we present our current server farm.
We are always searching for more and better hardware.
Please contact us if you think you could support us by sponsoring hardware.

## Mirror Server

Right now our mirrors are stored on a

  - Dell R720xd
  - 2 * Intel(R) Xeon(R) CPU E5-2640 0 @ 2.50GHz
  - 768 GiB RAM
  - ZFS with 24 * 6 TB SAS drives. 4 vdevs, each with 6 drives in RAID-Z2.
  - 2 x HP D2600, connected to a LSI SAS9207-8e HBA
  - Ubuntu Server 18.04

## Virtualization Servers

Two identical servers are running a visualization cluster

  - Dell R710
  - 2 * Quad Core Xeon E5540 2.53 GHz processor
  - 48 GiB RAM

One server has 4x 146 GB 15K SAS in raid 5, and the other one has 4x 2 TB 7.2K SATA in raid 5.

The services they run include:

  - Web-server (this site)
  - Monitoring (Observium)
  - Usenet server (Diablo on FreeBSD)
  - Tor relays
  - OpenStreetMap tile server
  - LineageOS build server
  - SheepIt render node

Every service runs isolated in LXC containers, with the only exception being the FreeBSD Usenet server that runs as a KVM.

## IRC Server

  - Dual Pentium III 800MHz
  - 1G RAM
  - [FreeBSD](https://www.freebsd.org/)
  - Located in Lyngby at [DeiC](https://deic.dk/)

## Switches and internet connection

  - 2x Cisco Catalyst 3750-X, both with 10G network modules (C3KX-NM-10G), and the "IP Services" license.

They are stacked and redundant, with one switch having a 10G connection to our ISP's (DeiC) router in Lyngby, and the other switch having a 10G connection to DeiC's router in Ørestad.
We announce our networks to DeiC using BGP.

Each server has at least one cable to each switch (bonded using LACP), which lets us restart a single switch without causing downtime.

## KVM

  - APC KVM switch with 19" rack screen & keyboard
  - Dell 2162DS remote KVM switch
  - APC PDU with remote power management
