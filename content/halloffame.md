+++
title = "Hall of Fame"
url = "aboutus/staff/halloffame/"
+++

## Claus Albøge (ClausA, tractrix)

Usenet

![](../../../img/staff/csa.jpg)
    
## Per Mejdal Rasmussen (PMR / Cablecat)

Network

![](../../../img/staff/pmr.jpg)


## Jacob Sørensen



![](../../../img/staff/jacob.jpg)


## Jener Rasmussen (jenrik)
    
![](../../../img/staff/jenrik.jpg)

    

## Thomas Lauridsen



![](../../../img/staff/trol.jpg)
    

## Thomas Rasmussen (simpsons)



![](../../../img/staff/simpsons.jpg)
    

## Thor Lange (lange)



![](../../../img/staff/lange.jpg)
    

## Uffe Overgaard Koch



![](../../../img/staff/uffe.jpg)
    

## Brian Grunnet



![](../../../img/staff/moonboot.jpg)
    

## Brian Jørgensen (qte)



![](../../../img/staff/qte.jpg)
    

## Bo Bai



![](../../../img/staff/bai.jpg)
    

## Christina Biangslev



![](../../../img/staff/thorne.jpg)
    

## Christian Reiniger



![](../../../img/staff/creinig.jpg)
    

## Claus Nyhus Christensen



![](../../../img/staff/santa.jpg)
    

## Eric Herring



![](../../../img/staff/herring.jpg)
    

## Flemming Kjær Jensen



![](../../../img/staff/fkj.jpg)
    

## Georg Sluyterman (sman)



![](../../../img/staff/sman.jpg)
    

## Jes Sørensen



![](../../../img/staff/jes.jpg)
    

## Jørgen B. Nielsen



![](../../../img/staff/jbn.jpg)
    

## Karsten Thygesen



![](../../../img/staff/karthy.jpg)
    

## Lau Bakman



![](../../../img/staff/bakman.jpg)
    

## Mads hugo Pedersen



![](../../../img/staff/madsp.jpg)
    

## Martin Kasper Petersen



![](../../../img/staff/mkp.jpg)
    

## Michael Collin Nielsen



![](../../../img/staff/michael.jpg)
    

## Mikael Hansen



![](../../../img/staff/mhansen.jpg)
    

## Morten Knud Nielsen



![](../../../img/staff/morten.jpg)
    

## Peter Korsgaard (Jacmet)



![](../../../img/staff/jacmet.jpg)
    

## Peter Kristensen



![](../../../img/staff/pkr.jpg)
    

## Simon Lyngshede (simonl)



![](../../../img/staff/simon.jpg)
    

## Simon Nybroe



![](../../../img/staff/john7doe.jpg)
    

## Steen Jensen



![](../../../img/staff/sj.jpg)
    

## Søren Staun-Pedersen



![](../../../img/staff/staun.jpg)
    

## Esben Haabendal (esben, bart)



![](../../../img/staff/esben.jpg)
    

## Søren Hansen (bonde)



![](../../../img/staff/shan.jpg)
    

## Michael Knudsen (mk, reverse)



![](../../../img/staff/mk.jpg)
    

## Martin Toft (mt)



![](../../../img/staff/mt.jpg)
    

## Martin Dalum (garfield)



![](../../../img/staff/garfield.jpg)


## Kim Højgaard-Hansen (kimrhh)



![](../../../img/staff/kimrhh.jpg)
