+++
title = "IRC"
+++

 IRC is an acronym for Internet Relay Chat - a place where people meet and chat with all sorts for people from all over the world.

If you want to connect to our IRC server you must connect to the following host and port:

- irc.dotsrc.org - port 6667

The objective of this server is to provide services for users in the Danish Kingdom (that is Denmark, Greenland and the Faeroe Islands) 
We are also providing service to the private nets at Aalborg University.

## Bots

Bots are automated IRC clients.
Bots can be useful to keep a channel open to people who visit it frequently.
In order to justify a bot there should be a sound user-base of frequent visitors.
Most bots are specially designed to withstand heavy attacks.

If you would like a bot to run on irc.dotsrc.org, then send a mail to irc-team@dotsrc.org giving a reason why you should be allowed to run a bot on dotsrc.org.
Specify which channel it should run on, the user-base of the channel and who is the owner of the bot.

## Client scannings

On join we connect to some ports on your host in order to scan for open proxies, trojans etc. to prevent hacked or virus infected hosts in connecting to our server.
We are not trying to hack you with those scans, but simply trying to prevent hackers in exploiting our services to their illegal purposes.

## Contact Information

If you have any problems with the IRC service, please contact us at irc-team@dotsrc.org.
Please do not ask questions about how to setup your IRC client, search engines are much better at answering those questions.
We are able to help you with:
- Abuse on IRC
- If you can not get on IRC and believe you should be able to
- Other matters regarding IRCnet in Denmark.

IRC operators (people running the service) are Dr\_Jazz, bonde and STL. They can be contacted either on irc-team@dotsrc.org or on IRC using their respective nicks. You can find all Danish opers on the channel #dk-opers. 
