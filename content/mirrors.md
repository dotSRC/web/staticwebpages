+++
title = "Mirrors"
+++

Dotsrc.org provides a set of mirrors of some of the most used and important Free and open-source software.
If you think we have missed a project let us know at staff@dotsrc.org and we will see what we can do.

## General Information

All mirrors are accessible via:
- HTTP at http://mirrors.dotsrc.org/
- HTTPS at https://mirrors.dotsrc.org/
- FTP at ftp://mirrors.dotsrc.org/
- Tor at our [onion service](http://dotsrccccbidkzg7oc7oj4ugxrlfbt64qebyunxbrgqhxiwj3nl6vcad.onion/)
- anonymous rsync and rsync over TLS at rsync://mirrors.dotsrc.org/

Please take a look at them to get an up-to-date list on what is available, and send us an email if we are missing something you would like to clone.

We are the main country mirrors for Debian (http://ftp.dk.debian.org) and Ubuntu (http://dk.archive.ubuntu.com).

All mirrors are updated at least daily.

## Mirroring from us

Fell free to pull any mirror from us. For a list of mirrors available via rsync, simply do a `rsync rsync://mirrors.dotsrc.org/`.

## rsync over TLS

rsync does by itself not support TLS, but we can simply outsource TLS handling to the `openssl s_client`.
The rsync authors have made a short script to do exactly that.
This is how you use it to get a listing from our server:

```sh
wget https://download.samba.org/pub/rsync/openssl-rsync
chmod +x openssl-rsync
rsync --rsh=./openssl-rsync rsync://mirrors.dotsrc.org 
```

It is wise to read the script before executing it blindly.
