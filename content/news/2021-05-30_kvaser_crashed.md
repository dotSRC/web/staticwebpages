+++
title = "Hardware failure on mirrors.dotsrc.org"
subtitle = "ETA: fixed"
date = "2021-05-30T20:04:17+02:00"
tags = ["downtime"]
author = "Anders Trier Olesen"
+++

mirrors.dotsrc.org was unavailable for about 2 hours, starting at 2021-05-30T19:51+02:00 until it became available again at 2021-05-30T21:56+02:00.

One of our HP D2600 disk enclosures had crashed, which took down the entire ZFS pool.

Update T20:45+02:00: We'll have a man on-site T20:45+02:00. If we're lucky mirrors.dotsrc.org should be back online shortly thereafter. If the disk enclosure is completely dead, ETA is unknown.

Update T22:20+02:00: Thomas was able to fix the problem! We're back online!
