+++
title = "Hardware failure on mirrors.dotsrc.org"
subtitle = "ETA: Fixed"
date = "2021-06-01T11:43:11+02:00"
tags = ["downtime"]
author = "dotsrc staff"
+++

mirrors.dotsrc.org is unavailable, starting at aprox 2021-06-01T11:30+02:00.
One of our HP D2600 disk enclosures had crashed, which took down the entire ZFS pool again.

Update 2021-06-03: Fixed by moving to a new location, see newer posts.
