+++
title = "Dotsrc is looking for IPv4 address space"
#subtitle = ""
date = "2021-07-06T17:08:54+02:00"
tags = ["tor", "sponsor"]
author = "dotsrc staff"
+++

Dotsrc have been running fast Tor relay nodes for the last couple of years. Last year we began testing if we could operate Exit nodes as well, and did so for about two months. The vast majority of exit traffic is of course benign, but there's also some malicious traffic which our ISP wasn't too happy about.

Since then we have had quite a few meetings and long email threads looking into possible solutions. As of now, the ideal solution seems to be to register Dotsrc with its own AS number and IP addresses. We can get IPv6 addresses directly from RIPE, but getting IPv4 addresses isn't as easy.

That is why we're currently looking for an IPv4 /24 that we can announce. If you know anyone who may have an IPv4 /24 that we can borrow (or they're willing to donate), don't hesitate to contact us at staff@dotsrc.org.
