+++
title = "Connectivity Issues"
date = "2022-02-09T22:28:05+02:00"
tags = ["downtime"]
+++

We are currently experiencing issues with our internet connection, which may cause connection failures.
From our testing this is only limited to IPv4.

We will update this post when we know more.

**Update 2022-02-10T21:13:55+02:00**:
It should now be possible to connect again via IPv4.
The issues was a configuration error at our ISP.

We would like thank DeiC for helping us resolve the issue this quickly.
