+++
title = "Power Outage"
date = "2022-04-13T14:38:15+02:00"
tags = ["downtime"]
+++

We are currently experiencing a power outage in Aalborg, and are therefore down.
We are sorry for the inconvenience this causes and hope that power will be back soon.

**Update**:
The outage has been resolved.
