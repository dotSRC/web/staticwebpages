+++
title = "Service Disruption"
date = "2023-11-21T18:14:00+01:00"
tags = ["disk"]
subtitle = "Disk failure"
author = "dotsrc staff"
+++

We are experiencing a disk failure. Which are causing NGINX to return
occasional 500 Internal Server Error codes.

We are aware of the issue and are working to procure a replacement
harddrive.
