+++
title = "Donation from PROSA"
date = "2025-01-15"
tags = ["sponsor"]
+++

<a href="{{< relURL "sponsors/#prosa" >}}">
<img src="{{< relURL "./img/sponsors/prosa.png" >}}" height="85" alt="Prosa logo"/>
</a>

PROSA east and west made a generous donation towards new server equipment.
We are truly grateful for this support, which will be used in a future upgrade to our network setup.
