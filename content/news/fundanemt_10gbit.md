+++
title = "10Gbit"
date = "2017-07-19"
+++
<div class="newsContent">
    The dotsrc servers are now backed by a 10Gbit Internet connection, and on top of this, the mirror server (
    <a class="normal" href="https://mirrors.dotsrc.org/">
     mirrors.dotsrc.org
    </a>
    ) has been upgraded to a vastly better server, equipped with a 10Gbit network card.
    <br/>
    <br/>
    This big upgrade would not have been possible without our sponsors. Big thanks to:
    <br/>
    <a class="normal" href="http://www.dkuug.dk/">
     DKUUG
    </a>
    - for a big cash sponsorship.
    <br/>
    <a class="normal" href="https://www.deic.dk/">
     DeIC
    </a>
    - 10Gbit Internet connection.
    <br/>
    <a class="normal" href="http://www.aau.dk/">
     Aalborg University
    </a>
    - Sponsored their old Google Search Appliance server, along with two Cisco 3750X switches + 10Gbit modules.
    <br/>
    <a class="normal" href="http://www.cisco.com/">
     Cisco
    </a>
    - Sponsored two IP Services licenses for our 3750X switches, allowing us to peer with DeIC using BGP.
    <br/>
    <a class="normal" href="https://fosdem.org/">
     FOSDEM
    </a>
    - Sponsored two HP 10GbE dual port network cards.
</div>
