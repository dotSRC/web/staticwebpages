+++
title = "20Gbit"
date = "2018-10-17"
+++
<div class="newsContent">
    At dotsrc.org we just got our internet bandwidth doubled. Not because we needed the capacity, but we wanted the redundancy.
    <br/>
    We now have two 10G uplinks. One to our ISP's (DeiC) router in Lyngby, and one to DeiC's router in Ørestad. Thanks to DeiC and AAU It Services for making this possible.
</div>
