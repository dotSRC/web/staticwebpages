+++
title = "Aalborg University donated USENET server"
date = "2004-04-26"
+++
<div class="newsContent">
    Aalborg University has donated a new dedicated USENET news server to SunSITE.dk. The server will be put in production on May 2nd 2004.
</div>
