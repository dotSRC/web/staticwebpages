+++
title = "Added https access to the mirror"
date = "2016-09-05"
+++
<div class="newsContent">
    Thanks to
    <a class="normal" href="https://letsencrypt.org/">
     Let's Encrypt
    </a>
    providing free SSL certificates, our mirror service is now available using TLS at
    <a class="normal" href="https://mirrors.dotsrc.org">
     https://mirrors.dotsrc.org
    </a>
    .
    <br/>
    We also updated our Usenet signup system, so if you have any problems signing up, please send an email to staff@dotsrc.org.
</div>
