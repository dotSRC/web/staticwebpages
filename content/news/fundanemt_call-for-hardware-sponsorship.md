+++
title = "Call for hardware sponsorship"
date = "2016-09-05"
+++
<div class="newsContent">
    During the recent hardware upgrade, we used all our 4TB drives, so we call for sponsors of additional hard drives in order to keep running our services as optimally as possible. Specifically we need SATA 3.5" hard drives 4TB (or more) and preferably designed for 24/7 usage (e.g. the Western Digital SE/RE/Gold 4TB drive). Sponsoring either as funding or as hardware will of course add you to our list of proud sponsors. Contact staff@dotsrc.org.
</div>
