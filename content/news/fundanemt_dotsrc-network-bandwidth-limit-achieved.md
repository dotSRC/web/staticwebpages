+++
title = "Dotsrc network bandwidth limit achieved"
date = "2010-02-21"
+++
<div class="newsContent">
    Some time ago our network connection was upgraded to 1Gbps via a dedicated fibre to the Danish Research Network.  However, not until Thursday this week did we manage to reach the bandwidth limit:  Over the past couple of months our average output has been circa 300Mbps, but on the 18th we were pushing around 980Mbps for about 2.5 hours, adding up to an impressive amount of data. The reason for this extraordinary amount of traffic was mostly due to new releases from Mozilla.
</div>
