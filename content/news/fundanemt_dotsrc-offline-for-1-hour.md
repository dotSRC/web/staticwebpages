+++
title = "Dotsrc offline for 1 hour"
date = "2018-04-09"
+++
<div class="newsContent">
    Due to some problems at our ISP (DeIC), all our services, except our IRC-server which is hosted elsewhere, was unfortunately offline about 1 hour from 2018-04-06 00:05:07 CEST until 2018-04-06 01:15:07.
    <br/>
    <br/>
    We apologize for any inconvenience.
</div>
