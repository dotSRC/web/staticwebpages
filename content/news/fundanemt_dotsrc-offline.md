+++
title = "Dotsrc offline"
date = "2013-06-24"
+++
<div class="newsContent">
    Due to an issue in the core of Danish E-infrastructure Cooperation (DeIC), formerly known as "Forskningsnet/Darenet" the services at dotsrc.org except irc.dotsrc.org which is hosted elsewhere, has unfortunately been offline from friday night until today this afternoon. The underlying problem has not been fully resolved yet, so some unstability may occur this evening. DeIC expects to have the issue resolved early Tuesday morning.
    <br/>
    <br/>
    We are sorry about any inconvenience this may have caused our users.
</div>
