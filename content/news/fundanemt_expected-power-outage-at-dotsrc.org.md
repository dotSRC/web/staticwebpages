+++
title = "Expected power outage at dotsrc.org"
date = "2009-08-26"
+++
<div class="newsContent">
    Thursday August 27th from 13.00 to 19.30 CEST: We expect to be offline at least one hour during several maintenances of the power distribution of the building we are hosted in.
</div>
