+++
title = "Further downtime and the news server"
date = "2013-06-28"
+++
<div class="newsContent">
    For a few days we have had some unstability due to a faulty fibre connection at DeIC, however that whas resolved wednesday. But murphy is around. Our electrical installation failed thursday at 20:24 and dotsrc.org (except irc.dotsrc.org) was offline from then until friday at 13:30 where all services except news.dotsrc.org was brought online again.
    <br/>
    <br/>
    news.dotsrc.org will be brought online again monday but is unfortunately unavailable until monday.
    <br/>
    <br/>
    We are sorry for the poor uptime the past week and expect much better uptime in the time to come.
</div>
