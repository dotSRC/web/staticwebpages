+++
title = "Hardware/Sponsor Request"
date = "2013-11-18"
+++
<div class="newsContent">
    We need additional hard drives for some of our servers in order to keep running our services as optimally as possible. Specifically we need SATA 3.5" hard drives as large as possible and preferably designed for 24/7 usage. Sponsoring either as funding or as hardware will of course add you to our list of proud sponsors.
</div>
