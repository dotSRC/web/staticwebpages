+++
title = "Hardware upgrade for mirrors.dotsrc.org"
date = "2009-03-26"
+++
<div class="newsContent">
    Mirrors.dotsrc.org has been down for maintenance today between 23:50 and 24:00. Thanks to
    <a class="normal" href="http://www.dkuug.dk">
     DKUUG
    </a>
    we have been able to upgrade the mirror server from 2 GiB RAM to 32 GiB RAM
</div>
