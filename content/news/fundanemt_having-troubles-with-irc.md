+++
title = "Having troubles with IRC?"
date = "2005-03-27"
+++
<div class="newsContent">
    On March 4th we stopped accepting clients on the old IRC server on sunsite.dk. We can see that some clients still try to connect to the old server instead of the new one at irc.dotsrc.org. So please, use the new server which is accessible from Danish addresses and from the networks at the dormitories at Aalborg University.
</div>
