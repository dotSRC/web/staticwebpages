+++
title = "High load today"
date = "2009-09-10"
+++
<div class="newsContent">
    We have recently become a Mozilla mirror and have now experienced the high load this gives on our network during the release of Firefox 3.5.3. It seems our firewall can not push more than 380 Mbps (3x our avg. load), while our mirror-server would be happy to serve more. We expect to have solved this bottle neg before the next release of Firefox, and apologize if the speed on our mirrors are not super speedy today.
</div>
