+++
title = "IPv4 problems"
date = "2019-02-12"
+++
<div class="newsContent">
    All dotsrc services were unavailable on IPv4 from around 2019-02-12 12:30 CET to around 16:00 CET. IPv6 traffic was unaffected.
    <br/>
    <br/>
    The outage was caused by BGP hijacking of 130.225.224.0/19 announced from an Iranian ISP.
</div>
