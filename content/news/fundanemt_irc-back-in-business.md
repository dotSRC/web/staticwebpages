+++
title = "IRC back in business"
date = "2005-05-22"
+++
<div class="newsContent">
    The IRC server stopped responding Thursday. Unfortunately, the server could not come up on its own and we were unable to handle the problem at the time. The service has since been restored and is now running properly. Thank you for your patience and sorry for the problems.
</div>
