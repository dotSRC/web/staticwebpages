+++
title = "Maintenance window for mirrors.dotsrc.org"
date = "2020-02-16"
+++
<div class="newsContent">
    We are seeing so much traffic that it is necessary to upgrade the hardware hosting mirrors.dotsrc.org.
    <br/>
    <br/>
    We'll be starting at 2020-02-18T08:00+01:00 (ISO8601), and we expect it to last up to three hours.
    <br/>
    <br/>
    To convert that to your local time run: date -d 2020-02-18T08:00+01:00
    <br/>
    <br/>
    If you want to get an email from us before the next scheduled maintenance, please let us know.
    <br/>
    <br/>
    Update: Upgrade went well. The server hosting mirrors.dotsrc.org now has 768Gb RAM instead of 96Gb. All this additional RAM will be used as ZFS ARC cache.
</div>
