+++
title = "Milestone reached: 100TB a week"
date = "2018-01-14"
+++
<div class="newsContent">
    The total amount of data we upload every week has just passed 100TB! On average this is more than 1.33Gbit/sec.
    <br/>
    <a class="normal" href="https://dotsrc.org/images/dotsrc_100TB.png">
     Traffic graph.
    </a>
    <br/>
    <br/>
    Our mirror service is the main contributer, which accounts for around 80TB.
    <br/>
    <br/>
    The last 20TB is shared between our other services (OpenStreetMap, Tor and Usenet).
</div>
