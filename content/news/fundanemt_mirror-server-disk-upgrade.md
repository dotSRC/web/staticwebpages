+++
title = "Mirror server disk upgrade"
date = "2010-03-20"
+++
<div class="newsContent">
    We have upgraded our mirror server with additional disks. 6 x 2 TB in a raid10, giving us 6 TB more storage and more I/O performance. Thank you to our sponsor
    <a class="normal" href="http://www.cabo.dk/ ">
     Cabo Communications
    </a>
    for the disks and Per Mejdal for a disk controller for the disks.
</div>
