+++
title = "New Firewall"
date = "2009-11-06"
+++
<div class="newsContent">
    Per Mejdal has kindly sponsored us with a new firewall, which should now make it possible for us to fill our 1 Gbps pipe to the Internet. The old firewall is being used in a redundant setup with the new firewall.
    <br/>
    We have also made some network changes this evening which, together with putting the new firewall into production, gave us some frequent outages throughout the evening.
</div>
