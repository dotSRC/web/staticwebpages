+++
title = "New IP address for mirrors.dotsrc.org"
date = "2008-08-11"
+++
<div class="newsContent">
    We have migrated our mirror service (http://mirrors.dotsrc.org / ftp://mirrors.dotsrc.org) to a new, faster and more spacious server. Thus the IP address of the service has changed to 130.225.254.116. If you have ACLs in place you should update them to reflect this IP change.
    <br/>
    <br/>
    In the not so distant future we will add more mirrors since we now have more space on the server. We will post news about this here.
</div>
