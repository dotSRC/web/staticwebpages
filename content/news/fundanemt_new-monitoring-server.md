+++
title = "New monitoring server"
date = "2012-11-27"
+++
<div class="newsContent">
    Today we have set up a new external Icinga monitoring server which can inform us of downtime via e-mails and sms's. This should enable us to notice downtime much faster and hence improve the uptime of our services.
</div>
