+++
title = "New name servers"
date = "2011-07-17"
+++
<div class="newsContent">
    We changed name servers for our .dk domains a few days ago. In that transition we made an error, which was corrected again, but some users may experience problems reaching the services from dotsrc.org for a period of time because of negative caching. We apologize for any inconvenience.
</div>
