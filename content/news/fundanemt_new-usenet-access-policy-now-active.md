+++
title = "New usenet access policy now active"
date = "2005-11-26"
+++
<div class="newsContent">
    As we announced earlier, our access policy to the usenet server would be changed on October 15th, 2005. Unfortunately there were some unexpected difficulties in implementing this policy in practice.
    <br/>
    <br/>
    The problems have now been solved, and as of tonight the new policy is active. In short this means that users connecting from a non-danish IP will only be able to access Open Source related groups and Danish news groups.
    <br/>
</div>
