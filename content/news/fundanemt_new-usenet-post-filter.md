+++
title = "New Usenet Post Filter"
date = "2006-08-21"
+++
<div class="newsContent">
    Today a new usenet post filter has been enabled on our news server. The primary purpose of this filter is to clean out SPAM - which we do not tolerate. The filter is rather aggressive and will also reject eg. HTML postings - so make sure your news reader is configured to post plain text articles. If you by any chance find that a legitimate, correctly formatted (See
    <a class="normal" href="http://rfc.sunsite.dk/rfc/rfc1036.html">
     RFC 1036
    </a>
    ) message from you gets rejected, please let us know.
    <br/>
    <i>
     Update:
    </i>
    Due to a misconfiguration, the first messages posted after the implementation were lost. The configuration has now been corrected, and all affected users have been notified.
</div>
