+++
title = "News Server IP address change"
date = "2009-06-22"
+++
<div class="newsContent">
    Within the next few days we are going to change the IP address of our news server. We have already moved the backend parts of the news server setup to a new IP range, and we will now continue with the frontend part.
    <br/>
    <br/>
    In general users shouldn't be affected by this change, but if you have been using our news service for a very long time, there is a chance that you news client might be configured to use "sunsite.dk" instead of "news.dotsrc.org" (or "news.sunsite.dk") as NNTP-server.
    <br/>
    <br/>
    So if you experience any problems - AFTER THE CHANGE - please make sure that your client is configured to use "news.dotsrc.org" as NNTP-server.
</div>
