+++
title = "News server upgraded"
date = "2004-08-22"
+++
<div class="newsContent">
    The news server will be updated next Saturday (2004-08-28) and thus a few hours of service downtime is to be expected.
    <br/>
    <br/>
    <u>
     Update: 2004-08-28
    </u>
    <br/>
    The news server was successfully upgraded and the news service was back online from around 1430, local time.
</div>
