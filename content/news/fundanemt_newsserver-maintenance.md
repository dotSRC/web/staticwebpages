+++
title = "Newsserver Maintenance"
date = "2005-05-28"
+++
<div class="newsContent">
    The newsserver will be down for maintenance  sunday may 29th. The service window is 1600-2200 CEST, but the actual downtime is expected to be 2 hours at max.
</div>
