+++
title = "Online again"
date = "2014-04-28"
+++
<div class="newsContent">
    Due to a damaged fibre between dotsrc and the DeIC POP about 100m away all our services, except our IRC-server which is hosted elsewhere, was unfortunately offline from 2014-04-26T19:52 until today at 15:56. We apologize for any inconvenience.
</div>
