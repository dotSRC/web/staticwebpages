+++
title = "Outages over the weekend"
date = "2006-11-09"
+++
<div class="newsContent">
    This weekend we'll be installing a few new servers (hooray), move some older server(s) around (uh, yay) and hopefully make our shiny Gbps uplink operational (doubleplusgood). That however means that we'll have to take some services offline for some time, and others might have unplanned outages. Please don't be mad at us -- we'll do our best, but some downtime is simply unavoidable.
    <br/>
    <br/>
    And things will be of course much better after the upgrade ;)
</div>
