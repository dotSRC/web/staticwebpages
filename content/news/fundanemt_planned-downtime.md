+++
title = "Planned downtime"
date = "2013-02-17"
+++
<div class="newsContent">
    This evening we are moving some servers between two racks and installing some new hardware. This means that the mirror service will be offline for about 10 minutes.
</div>
