+++
title = "Power outage at dotsrc.org"
date = "2009-07-28"
+++
<div class="newsContent">
    Starting yesterday at around 20:30 CET we had a power outage at campus which involved a rather hefty power surge. This resulted in all our machines were rebooted. We're almost back in the air again now. It seems like we didn't loose any hardware which we consider rather lucky considering the event. We'll keep you posted as we get the rest of our services running.
</div>
