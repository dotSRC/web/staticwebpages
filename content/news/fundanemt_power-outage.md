+++
title = "Power outage"
date = "2009-08-31"
+++
<div class="newsContent">
    We just had our third power outage for the last few weeks (this is not normal. We had servers with almost 4 years of uptime until a month ago). Some guy in the area cut a 20 kV line at 10:41 today, resulting in a 45 min. long power outage. Unfortunately our UPS did not last that long, resulting in 15 min. downtime for our services. Sorry for the inconvenience.
</div>
