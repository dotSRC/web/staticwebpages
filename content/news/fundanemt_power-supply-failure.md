+++
title = "Power supply failure"
date = "2008-06-06"
+++
<div class="newsContent">
    The power supply in one of our servers failed in the early hours of Thursday. A new power supply is on its way and we expect the server to be running early next week.
    <br/>
    <br/>
    The affected services are:
    <br/>
    <ul>
     <li>
      Mailing lists
     </li>
     <li>
      CVS
     </li>
     <li>
      Project web pages and project FTP
     </li>
    </ul>
</div>
