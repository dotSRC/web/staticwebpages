+++
title = "Problems solved"
date = "2009-07-28"
+++
<div class="newsContent">
    We had some routing issues as a result of the power cut yesterday which have now been resolved and everything should run as before.
    <br/>
    <br/>
    The power outage is according to information from the power supplier the result of a high voltage fault which resulted in a chain of 8 separate cable faults in the area.
    <br/>
    <br/>
    Power problems of this magnitude are very uncommon in the area. One of our servers reached an uptime of 1409 days.
</div>
