+++
title = "Project hosting discontinued"
date = "2009-10-21"
+++
<div class="newsContent">
    As of 2009-10-18 dotsrc.org has discontinued the hosting of projects. Since we started open source hosting more than 10 years ago, Sourceforge, Freshmeat, Launchpad and a bunch of others have emerged. Questions can be directed to
    <a class="normal" href="mailto:staff@dotsrc.org">
     staff@dotsrc.org
    </a>
    <br/>
    <br/>
    Please note that we will of course continue to run our open source mirror, usenet server, irc server, BitTorrent servers and so on. New services will emerge in the future.
</div>
