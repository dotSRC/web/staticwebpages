+++
title = "rsync over TLS"
date = "2020-01-21"
+++
<div class="newsContent">
    mirrors.dotsrc.org now support rsync over TLS.
    <br/>
    <br/>
    This is how you use it:
    <br/>
    <pre style="line-height:0.7em;">wget https://download.samba.org/pub/rsync/openssl-rsync
<br/>chmod +x openssl-rsync
<br/>rsync --rsh=./openssl-rsync rsync://mirrors.dotsrc.org</pre>
</div>
