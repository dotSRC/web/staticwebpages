+++
title = "Screenshots displaying configuration for USENET"
date = "2004-05-06"
+++
<div class="newsContent">
    We have added a new page with screenshots of news reader configuration examples.
    <br/>
    <br/>
    <a class="normal" href="/usenet/clientconfig/">
     See screenshots
    </a>
    .
</div>
