+++
title = "Server Breakdown Update #2"
date = "2007-09-11"
+++
<div class="newsContent">
    It is taking longer than expected to restore the data on the server. This is a combination of old hardware and a slow restore process.
    <br/>
    <br/>
    Short story, no mail or cvs service today. We will keep you updated.
</div>
