+++
title = "Server Breakdown Update #3"
date = "2007-09-12"
+++
<div class="newsContent">
    Unfortunately we've had a poweroutage which didn't affect our servers but it affected the backup systems which are in another building. This means that the data restore on the crashed disk array will be a bit more delayed.
    <br/>
    <br/>
    We will post updates as we get more news about this.
</div>
