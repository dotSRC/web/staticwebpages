+++
title = "Server Breakdown Update"
date = "2007-09-10"
+++
<div class="newsContent">
    Earlier today we managed (with help from KOM CWS) to get the failed server in a running state. Unfortunately one of the disk arrays had failed seriously, which means that data needs to be restored from backup. This will take some time, but with a bit of luck, we might be running again tomorrow (tuesday). Please check back here for updates.
    <br/>
</div>
