+++
title = "Server breakdown"
date = "2007-09-08"
+++
<div class="newsContent">
    Unfortunately one of our servers broke down last night. This means that mail-services is currently unavailable. We are working on a solution but is seems that we won't get everything back to normal before monday.
    <br/>
</div>
