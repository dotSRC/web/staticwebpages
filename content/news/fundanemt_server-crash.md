+++
title = "Server crash"
date = "2007-11-03"
+++
<div class="newsContent">
    One of our servers have crashed. This means that the following services are currently unavailable: project mailinglists, project websites and project cvs.
    <br/>
    <br/>
    We are working on fixing this issue as soon as possible but unfortunately it will probably take a couple of days.
    <br/>
    <br/>
    We are very sorry about this downtime and want you to know that we are doing everything in our power to improve on the situation. Next weekend we have planned a migration to some new servers which should improve both availability and performance.
</div>
