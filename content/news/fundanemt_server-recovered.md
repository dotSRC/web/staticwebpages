+++
title = "Server Recovered"
date = "2007-09-15"
+++
<div class="newsContent">
    Finally we got the last bits restored from tape.
    <br/>
    <br/>
    All services are online again, but in case of problems please report to staff@dotsrc.org, then we will fix them asap.
    <br/>
    <br/>
    Also please check your CVS repositories, the lastest commits before the crash might not have made it to our backup. You will have to recommit.
    <br/>
    <br/>
    The same goes for mail, any mail that hasn't come through yet, needs to be resent.
    <br/>
    <br/>
    Again, we are terribly sorry about this mess. During the last ~10 years of SunSITE.dk/dotsrc.org this is by far the worst breakdown. The restore took way longer than expected, unfortunately we can't really do otherwise with our current hardware. (Hint: a couple of new servers would be much appreciated - mail to staff@dotsrc.org if you got a spare server)
    <br/>
    <br/>
    A great thank you goes to the great people at KOM CWS for their massive help with the restore.
    <br/>
    <br/>
    <br/>
    <br/>
</div>
