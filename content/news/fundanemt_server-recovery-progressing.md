+++
title = "Server Recovery Progressing"
date = "2007-09-13"
+++
<div class="newsContent">
    The recovery of the crashed server is now nicely on its way.
    <br/>
    <br/>
    As a starter mail and mailinglists are now working again. We have quite a number of mails in the queue on our mailgateway which are now under delivery. Some mails have been temporarily rejected. These mails should start flowing in within the next day.
    <br/>
    <br/>
    CVS and project web sites are also on their way. More info later.
</div>
