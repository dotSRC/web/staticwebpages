+++
title = "Services up again"
date = "2005-03-18"
+++
<div class="newsContent">
    The servers were shutdown tonight due to over heating of the server room. All services are fully operational and up-to-date again. We're still working on tracing down the exact reasons for why the mirror server refused to come up again after the outage, so there might be another reboot needed in the near future. This time we're better prepared though, so that should only result in a few minutes of downtime.
    <br/>
    <br/>
    Sorry for the problems.
</div>
