+++
title = "Sponsored equipment"
date = "2014-03-28"
+++
<div class="newsContent">
    We have received some hardware that we will implement into our infrastructure:
    <br/>
    <br/>
    Prevas has sponsored us with two 4 TB disks and Xena has sponsored us with four 4 TB disks, an IP KVM switch and a number of servers, which we will be using for virtualization, new backup server, firewalls and more.
    <br/>
    <br/>
    We would like to thank Xena and Prevas for their support!
</div>
