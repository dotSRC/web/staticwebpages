+++
title = "Switch"
date = "2010-08-25"
+++
<div class="newsContent">
    Our main switch broke down today, resulting in our services to be unavailable from 8:25 to 11:15.  The switch has been replaced and all services are available again.
</div>
