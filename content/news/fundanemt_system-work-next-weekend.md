+++
title = "System work next weekend"
date = "2007-11-05"
+++
<div class="newsContent">
    Because of an overhaul of large parts of our systems some outages is expected at random points in time (sorry, can't be more specific about this).
    <br/>
    <br/>
    On the good side of things - after the weekend we should end up with some nicer systems :)
</div>
