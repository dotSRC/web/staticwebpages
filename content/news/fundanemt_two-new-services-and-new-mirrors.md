+++
title = "Two new services and new mirrors"
date = "2016-09-12"
+++
<div class="newsContent">
    We now run a tile caching server for
    <a class="normal" href="https://openstreetmap.org/">
     OpenStreetMap
    </a>
    and are a part of their tile CDN. We are the primary server for tiles served to clients connecting from Denmark, so if OpenStreetMap feels 'snappier' (at least in Denmark) it is thanks to us.
    <br/>
    <br/>
    We also set up a tor middle relay. This is a non-exit relay, so it only receives encrypted traffic and passes it along to another relay. Some stats are available
    <a class="normal" href="https://torstatus.blutmagie.de/router_detail.php?FP=9c4daed4759aa66d0e93eb27093bd21cfe2c2271">
     here
    </a>
    .
    <br/>
    <br/>
    On our mirror server, we added a few new mirrors, including:
    <br/>
    <a class="normal" href="https://mirrors.dotsrc.org/linuxmint-cd/">
     Linux Mint
    </a>
    (and
    <a class="normal" href="https://mirrors.dotsrc.org/linuxmint-packages/">
     Packages
    </a>
    )
    <br/>
    <a class="normal" href="https://mirrors.dotsrc.org/blackarch/">
     BlackArch Linux
    </a>
    <br/>
    <a class="normal" href="https://mirrors.dotsrc.org/tails/">
     Tails
    </a>
    <br/>
    <a class="normal" href="https://mirrors.dotsrc.org/openvz">
     OpenVZ
    </a>
    <br/>
    <a class="normal" href="https://mirrors.dotsrc.org/gimp">
     GIMP
    </a>
    <br/>
    <a class="normal" href="https://mirrors.dotsrc.org/fedora">
     Fedora
    </a>
    (and
    <a class="normal" href="https://mirrors.dotsrc.org/fedora-epel">
     Extra Packages for Enterprise Linux
    </a>
    )
    <br/>
    <a class="normal" href="https://mirrors.dotsrc.org/lineageos/full">
     Lineage OS
    </a>
    <br/>
    <a class="normal" href="https://mirrors.dotsrc.org/opensuse">
     openSUSE
    </a>
    <br/>
    <a class="normal" href="https://mirrors.dotsrc.org/kali-images/">
     Kali Linux
    </a>
    (and
    <a class="normal" href="https://mirrors.dotsrc.org/kali/">
     Packages
    </a>
    )
</div>
