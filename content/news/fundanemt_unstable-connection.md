+++
title = "Unstable connection"
date = "2013-07-02"
+++
<div class="newsContent">
    Our ISP (DeIC, formerly known as "Forskningsnettet") is working at resolving the issue. This is related to the issues we saw last week. Right now it means that we have a cycle of 1-2 hours downtime followed by 1-2 hours uptime :-/
</div>
