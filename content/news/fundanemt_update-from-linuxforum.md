+++
title = "Update from LinuxForum"
date = "2005-03-03"
+++
<div class="newsContent">
    We're just bringing you a small update from LinuxForum. Yesterday we stood by the HP stand all day, being very business like. The guy from HP was great fun, so was the magician HP had hired. We talked to a lot of potential sponsors, most seems to be interested, but we'll see when we follow up. We did however get a new server from the danish
    <a class="normal" href="http://www.gratisdns.dk/">
     GratisDNS.dk
    </a>
    , a free dns service.
    <br/>
    <br/>
    Just for the fun of it, we went to see John "Maddog" Hall. Michael shot this picture of Søren and Simon.
</div>
