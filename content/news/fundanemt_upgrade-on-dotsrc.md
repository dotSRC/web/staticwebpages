+++
title = "Upgrade on mirrors.dotsrc.org"
date = "2016-08-06"
+++
<div class="newsContent">
    The server hosting mirrors.dotsrc.org has been upgraded with newer and faster hard drives. The software stack has also been updated, which includes a switch from using the Apache HTTP server to Nginx.
</div>
