+++
title = "Usenet access change underway"
date = "2005-10-01"
+++
<div class="newsContent">
    Starting October 15th, 2005 the number of usenet groups will be reduced in order to make the main focus of the usenet service consistent with dotsrc.org's remaining policy and focus.
    <br/>
    <br/>
    In short, this means that non-danish users can only access Open Source related news groups, as well as danish news groups.
    <br/>
    <br/>
    Read our new
    <a class="normal" href="/usenet/accesschange/">
     access policy
    </a>
    , our
    <a class="normal" href="/usenet/ossgroups/">
     list of news group hierarchies
    </a>
    we provide and our
    <a class="normal" href="/usenet/policy/">
     policy and rules page
    </a>
    .
</div>
