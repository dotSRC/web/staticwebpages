+++
title = "Usenet server network changes"
date = "2009-10-17"
+++
<div class="newsContent">
    We have made some network changes for our Usenet server. This means that if you use "sunsite.dk" as server name in your config, you will have to change it news.dotsrc.org. If you use news.sunsite.dk your client will continue to work, but we encourage you to change it to news.dotsrc.org.
</div>
