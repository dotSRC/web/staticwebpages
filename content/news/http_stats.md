+++
title = "Statistics for Web Traffic"
subtitle = "WIP statistics for HTTP and HTTPS requests"
date = "2021-03-02T23:00:00+01:00"
+++

Since September we have been working on collecting usage statistics for our HTTP and HTTPS mirror traffic.
This will give insight into which folders and files are accessed often.

We currently collect the following values from each request:

- Path relative to mirrors.dotsrc.org
- Downloaded size

It is implemented with the following nginx log pattern:

```nginx
log_format logserver '$bytes_sent $uri';
```

More info can be found [here]({{< ref "/statistics" >}}).

