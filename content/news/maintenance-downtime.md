+++
title = "Upcoming Maintenance window"
subtitle = "We will be updating our servers"
date = "2020-10-30T11:00:00+02:00"
tags = ["downtime"]
+++


We'll be starting at 2020-10-30T14:15+02:00 (ISO8601), and we expect it to last 20 minutes for each server. 


If you want to get an email from us before the next scheduled maintenance, please let us know. 


EDIT: We didn't manage to update today due to non-technical issues.
We will reschedule and hopefully get it done within a month.
