+++
title = "dotsrc are moving to a new location"
subtitle = ""
date = "2021-06-01T15:31:07+02:00"
tags = ["downtime","tor","website","all"]
+++


The time has come for dotsrc to move our servers from our current server room at Aalborg University to a new one. 
The old locations had been decommissioned and were no longer maintained. 
Without adequate cooling, the servers have begun to behave rather strangely. Thus the relocation has been expedited.

We hope that all services are up and running again during Thursday afternoon.
