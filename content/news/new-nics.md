+++
title = "We Have Recieved New Network Adapters"
subtitle = "Thanks to our sponsor BrightStar Systems"
date = "2021-04-08"
tags = ["sponsor"]
+++

<a href="{{< relURL "sponsors/#brightstar-systems" >}}">
<img src="{{< relURL "./img/sponsors/brightstar.jpg" >}}" height="85" alt="BrightStar Systems logo"/>
</a>

{{< columns >}}
<img src="{{< relURL "./img/sponsors/brightstar-received-flowers.jpg" >}}" height="250" alt="Picture of network adapters"/>
{{< column >}}
<img src="{{< relURL "./img/sponsors/brightstar-received-davinci.jpg" >}}" height="250" alt="Picture of network adapters"/>
{{< endcolumns >}}

BrightStar Systems sponsored 2 Intel X520-DA2.


We have been having some trouble with our current network cards (we are currently running at 1x10G) and will use these to replace them (restoring our previous 2x10G) :)
