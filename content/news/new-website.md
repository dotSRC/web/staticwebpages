+++
title = "The New Website is Live"
subtitle = "If You See This on dotsrc.org"
date = "2020-09-22"
tags = [ "website" ]
author = "Victor Büttner"
+++

We finally migrated away from the old unmaintained PHP CMS.
As you can see in the footer, we are now using Hugo to generate static pages.


## What is New for Visitors?

### RSS
Thanks to Hugo and our theme, you can now get news via RSS.
The icon in the footer links to the feed!

### Theme
We are trying keep the spirit of the old site intact, but it seems none of us are super interested in frontend development.
Therefore we are using a fork of an existing theme which helps us out with with a few things we can't/won't do ourselves:
- SEO
- Mobile friendlyness
- Columns, galleries, etc.

It does come a few cons::
- Disabling javascript slightly degrades visual elements
- The size of the website has increased (mostly due to images and javascript)

If the cons bother you, you can make suggestions to fix them via email, -an issue, or create a merge request.


As a part of this change the structure of the site was also changed and it is hopefully easier to navigate.
Fortunately, bookmarks won't break since the same links will (mostly) go to the same page.


## Did We Miss Something?
In case you notice that we have overlooked some content from the old site please don't hesitate to send us an email, open an issue, or create a merge request.
