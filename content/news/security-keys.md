+++
title = "dotsrc will now use security keys"
subtitle = "Thanks to our sponsors SoloKeys and NitroKeys"
date = "2020-10-07T21:24:45+02:00"
tags = ["sponsor"]
+++

{{< columns >}}
<a href="{{< relURL "sponsors/#solokeys" >}}">
<img src="{{< relURL "./img/sponsors/solokeys.webp" >}}" height="50" alt="SoloKeys logo"/>
</a>

<img src="{{< relURL "./img/sponsors/solokeys-received.jpg" >}}" height="250" alt="Picture of SoloKeys"/>
SoloKeys sponsored 4 Solo USB-C and 6 Solo USB-A.
</figure>
{{< column >}}
<a href="{{< relURL "sponsors/#nitrokey" >}}">
<img src="{{< relURL "./img/sponsors/nitrokey.png" >}}" height="50" alt="Nitrokey logo"/>
</a>

<img src="{{< relURL "./img/sponsors/nitrokeys-received.jpg" >}}" height="250" alt="Picture of Nitrokeys"/>
Nitrokey sponsored 5 Nitrokey FIDO2.
{{< endcolumns >}}

We will be using these primarily for SSH and various webservices.
