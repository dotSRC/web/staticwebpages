+++
title = "dotsrc have successfully moved to the new location"
subtitle = "mirrors.dotsrc.org should be back up and running"
date = "2021-06-03"
tags = ["downtime","tor","website","all"]
+++

We have successfully moved to new location and mirrors.dotsrc.org should be up and running.
Please write us on staff@dotsrc.org if you do not think this is the case and we will take a look :)

Note that one of our virtualization servers still is down at the time of writing, see [this post](/news/vm1_downtime).

Thanks to Aalborg University for supplying us with one more server, assistance, and still accomodating us.

Below are some pictures of the move, we will spare you the details.

{{< gallery >}}
  {{< figure src="/img/move-to-cass/trolley.jpg" caption="Moving equipment on a trolley" >}}
  {{< figure src="/img/move-to-cass/almost-empty-rack.jpg" caption="The almost empty rack" >}}
  {{< figure src="/img/move-to-cass/wo-cables.jpg" caption="Mounted some equipment" >}}
  {{< figure src="/img/move-to-cass/kvm-up.jpg" caption="Checking that nothing went wrong" >}}
  {{< figure src="/img/move-to-cass/replacing-nic.jpg" caption="Replacing network cards" >}}
  {{< figure src="/img/move-to-cass/full-rack.jpg" caption="Complete with new server and new network cards" >}}
{{< /gallery >}}


