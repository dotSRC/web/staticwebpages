+++
title = "Our Tor nodes no longer accepts exit traffic"
date = "2020-11-22"
tags = ["tor"]
+++

Our ISP DeiC has decided to ban hosting of Tor exit nodes directly on the Danish national research and education network.

All hope is not lost - we're still working with DeiC to find a solution.

In the meanwhile, please consider running a Tor node yourself (exit if you can, non-exit otherwise). Check out the [Tor Relay Guide](https://community.torproject.org/relay/) to get started.
