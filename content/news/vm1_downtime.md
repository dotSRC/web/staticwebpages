+++
title = "Hard disk failure resulted downtime"
date = "2021-05-09"
tags = ["downtime", "website", "tor"]
+++

After a hard disk failure on one of our virtualization servers, the following services have been down since 2021-05-02 10:20:07:

- Website
- Tor relay
- LineageOS build server
- OpenStreetMap tile server

As a temporary measure the website has been moved to Gitlab pages until the issues have been resolved.
