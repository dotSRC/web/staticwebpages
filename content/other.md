+++
title = "Other Projects"
aliases = [
	"projects",
]
+++

As of April 2009 we have shut down the general project hosting part. Since we started open source hosting more than 10 years ago, Sourceforge, Freshmeat, Launchpad and a bunch of others have emerged and are offering the same services.
We don't see the big need for us doing this any more, and will focus on other services.

As of 2009-10-18 all project hosting is discontinued.
If you have any questions regarding the shutdown process of the open source hosting, please contact staff group.

Please note that we will of course continue to run our mirror, Rsenet server, irc server and so on.
New services may emerge in the future. 
