+++
title = "Sponsors"
+++

dotsrc relies on sponsors donating hardware, service agreements, licences, housing and a lot more, and without our sponsors we would not be able to deliver our services to the Open Source community. 

## Aalborg University

<img src="../img/sponsors/aau.svg" height="200" alt="Aalborg University logo"/>

The university is one of our oldest supports. Since the beginning they have been providing us housing for the servers and backup.

This is just a short list of things the university has provided:

- Housing
- Backup
- A new news server
- A new mirror server
- Switches
- Server rack
- Cables, converters, all kinds of small indispensable things.
- Generally being extremely nice people 

Website: [aau.dk](https://www.aau.dk)


## Danish e-Infrastructure Cooperation

<img src="../img/sponsors/deic.svg" height="50" alt="Danish e-Infrastructure Cooperation logo"/>

Originally the university paid for our bandwidth, but in the later years DeiC (the university's ISP) has taken over. We have gone from having 33mbps to 20Gbps and from sharing a line with the university to having our own.

Website: [deic.dk](https://www.deic.dk/)


## Dansk Unix User Group

<img src="../img/sponsors/dkuug.svg" height="50" alt="Dansk Unix User Group logo"/>

In 2008 DKUUG sponsored a brand new storage server to help improve our mirror service. Again in 2017 DKUUG sponsored a big upgrade of our mirror server. 

Website: [dkuug.dk](http://www.dkuug.dk)


## PROSA

<img src="../img/sponsors/prosa.png" height="52" alt="PROSA logo"/>

Prosa sponsored six 8TB hard drives in December 2017.

Again in 2024 we got sponsorships from both PROSA/VEST and PROSA/ØST.

Website: [prosa.dk](https://www.prosa.dk/)

## BrightStar Systems

<img src="../img/sponsors/brightstar.jpg" height="85" alt="BrightStar logo"/>

In 2021 BrightStar Systems sponsored 2 Intel X520-DA2.

Website: [brightstarsystems.com](https://brightstarsystems.com/)

## SoloKeys

<img src="../img/sponsors/solokeys.webp" height="50" alt="SoloKeys logo"/>

In 2020 SoloKeys sponsored 4 Solo USB-C and 6 Solo USB-A.

Website: [solokeys.com](https://solokeys.com/)


## Nitrokey

<img src="../img/sponsors/nitrokey.png" height="50" alt="Nitrokey logo"/>

In 2020 Nitrokey sponsored 5 Nitrokey FIDO2.

Website: [nitrokey.com](https://www.nitrokey.com/)

## APC

<img src="../img/sponsors/apc.png" height="50" alt="APC logo"/>

When our old UPS failed in 2004, we asked APC to donate a new and larger UPS. APC did more than, not only did we get a very large and nice UPS with management interface, we also got some much needed hardware for managing our servers.


- UPS + extra battery (APC Smart-UPS)
- RackMount KVM switch
- 1U RackMount TFT-screen terminal 

Website: [apc.com](https://www.apc.com/)


## Cabo Communications

<img src="../img/sponsors/cabo.png" height="50" alt="Cabo Communications logo"/>

Cabo Communications was kind enough to donate six 2 TB disks for our mirror server.


## Cisco Danmark 

<img src="../img/sponsors/cisco.png" height="50" alt="Cisco Danmark logo"/>

Sponsored two IP Services licenses for our 3750-X switches in April 2017. 


## De Telefoongids & Gouden Gids

<img src="../img/sponsors/detelefoongids.jpg" height="50" alt="De Telefoongids & Gouden Gids logo"/>

De Telefoongids & Gouden Gids has sponsored us for equipment.


##  Xena

<img src="../img/sponsors/xena.jpg" height="50" alt="Xena logo"/>

Xena - an online accounting company, has generously sponsored us equipment: 
 - 4x4 TB Sata disks
 - Dell IP KVM unit (KVM2162DS)
 - 2x Dell R710
 - 6x Dell 2950 


## Epoka A/S

<img src="../img/sponsors/epoka.png" height="50" alt="Epoka logo"/>

 The university has been given more than fair prices when buying equipment for dotsrc.org/sunsite.dk from the Epoka Group. Thank you. 

Website: [epoka.com](https://www.epoka.com/)


## Heinex

<img src="../img/sponsors/heinex.png" height="50" alt="Heinex logo"/>

Heinex gave us a very good deal on the new storage server, that enabled DKUUG to buy it for us. 

Website: [heinex.dk](https://www.heinex.dk/)


## Hitflip

<img src="../img/sponsors/hitflip.png" height="50" alt="Hitflip logo"/>

Hitflip has sponsored disks for our new mirror storage server. 


## HP Danmark

<img src="../img/sponsors/hp.png" height="50" alt="HP Danmark logo"/>

In 2004/2005 HP stepped in providing us with the hardware needed to increase the number of mirrors we provide and a server for running the future compile server. The same server will be the login for our hosted projects in the future.
- A dual 1,3GHz Itanium2 server (HP Integrity rx2600-2) with 4GB RAM
- 1TB of storage in raid-5 via a HP Smart Array controller 

All hardware sponsored by HP is delivered with a 3-year next-day hardware support contract. 

Website: [hp.com](https://www.hp.com)


## Netic

<img src="../img/sponsors/netic.png" height="50" alt="Netic logo"/>

Netic pays the registration fee for all of our domain names. 

Website: [netic.dk](https://www.netic.dk/)


## Prevas A/S

<img src="../img/sponsors/prevas.jpg" height="50" alt="Prevas A/S logo"/>

Prevas has sponsored us with two 4 TB HDD for our virtualisation servers. 


## Spar Nord

<img src="../img/sponsors/sparnord.png" height="50" alt="Spar Nord logo"/>

Spar Nord Sponsored us a Sun rack with two Sun 280R and two StorEdge A1000. 

Website: [sparnord.dk](https://www.sparnord.dk/)


## Terabit Systems

<img src="../img/sponsors/terabit.jpg" height="50" alt="Terabit Systems logo"/>

Terabit Systems, a specialized reseller of network hardware, has sponsored us with a Dell PowerEdge 2950 ( 2x Dual Core 3.2 GHz Xeon, 8 GiB RAM). 

Website: [terabitsystems.com](https://www.terabitsystems.com/)


## TopDir

<img src="../img/sponsors/topdir.jpg" height="50" alt="TopDir logo"/>

TopDir a human edited directory, has sponsored us for equipment. 


## FOSDEM

<img src="../img/sponsors/fosdem.png" height="50" alt="FOSDEM logo"/>

FOSDEM sponsored two HP CN1000E 10GbE network adapters in April 2017. 

Website: [fosdem.org](https://fosdem.org/)
