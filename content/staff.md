+++
title = "Staff"
url = "aboutus/staff/"
+++

Dotsrc.org is managed by a group a volunteers, called the staff group.

The staff group can be reached at [staff@dotsrc.org](mailto:staff@dotsrc.org) and will try to answer any questions you might have about dotsrc.org and its services.

We are volunteers and not paid, thus support is done in our spare time, so extensive service and support might not be available at all times.
Our FAQ might answer your questions.

## Anders Trier Olesen (ato)

mirror, tor, openstreetmap, network

![](../../img/staff/ato.jpg)
    

## Lars Hausmann (LCH, Dr_Jazz)

IRC

![](../../img/staff/jazz.jpg)
    



## Sonny Larsen (STL)

IRC, Usenet

![](../../img/staff/stl.jpg)

## Victor Büttner (vdot0x23 / vb)

Anything but not everything

![](../../img/staff/victor.png)

## Julian Teule (jtle)

Mirror

![](../../img/staff/jtle.jpg)


## Thomas Lundsgaard Kammersgaard

Helpful with most things

![](../../img/staff/tlh.png)

## Falke Carlsen (falkecarlsen)

Maintenance

![](../../img/staff/falke.jpg)

## Kristian Mide

Enterprise

![](../../img/staff/mide.jpeg)