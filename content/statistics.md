+++
title = "Statistics"
subtitle = "Statstics for a selection of the services provided at dotsrc"
+++

## Network Statistics

- [10G link to DeIC's Lyngby router (lgb)](https://info.net.deic.dk/traffic/1e6421ce053562956073f92eae75caf7.html)
- [10G link to DeIC's Lyngby router (ore)](https://info.net.deic.dk/traffic/b865614a63dd398c81a6d69db70a5bb3.html)

## News Server Statistics

- [Usenet](http://brage.dotsrc.org/stats/)

## HTTP(S) Statistics

Every request to a file on <http://mirrors.dotsrc.org> is logged with the path and downloaded size.
These are accumulated and saved daily as json files which can be downloaded at <http://mirrors.dotsrc.org/status/statfiles>.

These are not very browser friendly, so a work-in-progress viewer React.js program has been created to read them ([found here](http://mirrors.dotsrc.org/status/stat_view.html)).
The filename can be filed in with a json file in the `statfiles` folder, such as `statfiles/day2020-12-31.json`.

### Program sources

- [Accumulator/counter program](https://gitlab.com/dotSRC/mirror_stats/-/tree/master/counter)
- [Merger](https://gitlab.com/dotSRC/mirror_stats/-/tree/master/merge/merger): Internally accumulated trees are saved every hour, so these are merged to create one file per day.

- [Viewer](https://gitlab.com/dotSRC/stat_viewer)
