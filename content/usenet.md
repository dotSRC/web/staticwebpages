+++
title = "Usenet"
aliases = [
    "/usenet/ossgroups",
    "/usenet/policy",
    "/usenet/clientconfig",
]
+++

dotsrc runs a Usenet newsserver with public access to local, danish, and some international newsgroups, and closed access to Aalborg University campus newsgroups.

This guide explains how to access the newsserver, both for campus users and others.

The server is provided in collaboration with Aalborg University, which have kindly paid for the server and the dotsrc.org staff group is in charge of administration.

## Status

Status, news, notices and rules of the usenet newsserver can be found [here](http://news.dotsrc.org/).

## Registration

To be able to use the new usenet server (read and post articles) you must register!

Just send an empty email to: news-signup@sunsite.dk and we will email you a username (which is your email) and a password.

This username/password pair must be entered in your newsreader and then you will have access to read and post articles. 
Help to configure your newsreader can be found [here](##usenet-client-configuration).

Please notice:
- If you're using this server, you accept our rules of usage!
- There is no right to be granted access. Access can be withdrawn at any time without further notice.
- Your email address will be put into the X-Trace: header of your postings, but to keep it from being harvested, this header will be encrypted.
- Our privacy policy is that your news registration will not be sold, published or used for anything except for validation against our newsserver.

## Policy

Dotsrc.org has a strong policy against any binary material or HTML in usenet postings.
We believe that Usenet is for communication - not for binary exchange.

As a consequence of this, we do not carry any binary group and we do have a rather aggressive anti-spam filtering system which also cleans out binaries, HTML postings etc.

The filter also ensures compliance with RFC1036 - especially with regard to using Re: (and not Sv:) in the Subject line in replies to postings.

## Rules

Users of the Dotsrc.org USENET news server are required to follow this set of rules.
Disregarding the rules may cause termination of access privileges without further notice!

**Real Name:**
It is required to use the accurate personal name (also called "real name") in each posting, particularly in the From: or Sender: field. It is not sufficient to include your name in the signature solely.

**Accurate Sender Address:**
The e-mail address in From: or Sender: fields must belong to you. Using identifiers of other individuals is not permitted.

**Netiquette:**
Most hierarchies of Usenet have their own netiquette and guidelines. Users should pay attention to the relevant policies.

**No Commercial Usage:**
This server is liable to the rules of Darenet, the Danish research Nework. Therefore particularly commercial or for-profit activities are prohibited.

**No SPAM:**
It is inadmissible to use this server to post SPAM. (To spam Usenet means to send many identical or nearly-identical messages separately to a large number of Usenet newsgroups.)

**No Control Messages:**
Users are not allowed to post control messages at this server. The only exception is to cancel your own articles.

**No Third-Party Cancels:**
No user is allowed to cancel any article other than his or her own.


## Open Source related groups

Starting October 15, 2005, dotsrc.org limits the provided groups to global users to reflect our policy to support Open Source development.
We will therefore only provide groups which are Open Source related or which can gain the developers of Open Source software.
Furthermore the primary language in the provided groups must be either English or Danish in order for the dotsrc.org staff group to be able to handle abuse cases.

At the moment the following hierachies are covered as Open Source related groups:

  - alt.comp.\*
  - alt.os.\*
  - comp.\*
  - emacs.\*
  - fm.\*
  - gnu.\*
  - linux.\*
  - microsoft.\*
  - netscape.\*
  - opera.\*
  - redhat.\*
  - staroffice.\*
  - sunsite.\*


If you miss any groups, which fullfill our criterion for an open source related group, please inform us, and we will judge whether we will add the group to our server or not.


## USENET Client Configuration

### General Information

When configuring your news reader to connect to the usenet news server at news.sunsite.dk you should generally use these configurations:

```
Server Name : news.dotsrc.org (formerly news.sunsite.dk - which wil continue to work.).
Port Number : 119
SSL : No
Authentication: Yes
```

The username and password for authentication is retrieved as described [here](../).

Below we have gathered some screenshots of configuration examples for some common news readers.

### Outlook

![](../img/usenet/oe1.png)
![](../img/usenet/oe2.png)

### Netscape / Mozilla / Thunderbird

![](../img/usenet/mozilla1.png)
![](../img/usenet/mozilla2.png)
![](../img/usenet/mozilla3.png)
![](../img/usenet/mozilla3.png)
![](../img/usenet/mozilla4.png)
![](../img/usenet/mozilla5.png)

When you have finished the setup guide click "manage newsgroup subscriptions".
Shortly you will be prompted for your username and password.
Remember to tell mozilla to remember your username and password.

![](../img/usenet/mozilla6.png)

### Opera

No screenshots available at the moment.

### Agent

![](../img/usenet/agent.png)

### Sylpheed

No screenshots available at the moment

### Gnus

Put the following into your `.authinfo` file in your home dir:

```
machine news.sunsite.dk login user@host.tld password 123456 force yes
```

And put the following two lines into your `.gnus` file:

```
(setq gnus-nntp-server "news.sunsite.dk")
(setq nntp-authinfo-function 'nntp-send-authinfo)
```

### Pan

![](../img/usenet/pan1.png)

### Gravity

![](../img/usenet/gravity.png)

### Slrn

No screenshots available at the moment
