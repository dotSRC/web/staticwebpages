import re
import fileinput
import json

regx = [
        r"\/fundanemt\/site_graphics\/staffmembers\/([^\"]*)",
        r"<h3>([^<]*)<\/h3>",
        r"<p>([^<]*)<\/p>"
        ]
fnames = ["nick", "name", "text"]

def format(user):
    return f"""
## {user["name"]}

{user.get("text", "")}

![](../../../img/staff/{user["nick"]})
    """

ulist = []

user = None
for line in fileinput.input():
    for i, r in enumerate(regx):
        res = re.search(r, line)
        if not res:
            continue

        # Wierd state machine
        if i is 0:
            if user:
                ulist.append(user)
                print(format(user))
            # First state
            user = {}
            state = 1

        user[fnames[i]] = res[1]

if user != None:
    ulist.append(user)
    print(format(user))

# print(json.dumps(ulist))
