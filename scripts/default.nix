let
  pkgs = import <nixpkgs> {};
  stdenv = pkgs.stdenv;

in stdenv.mkDerivation {
  name = "news-env";

  buildInputs = with pkgs; [
    python38
    python38Packages.beautifulsoup4
    python38Packages.dateutil
  ];
}
