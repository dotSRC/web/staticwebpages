#!/usr/bin/env python3


from bs4 import BeautifulSoup
import sys
import dateutil.parser

fileIn = open(sys.argv[1])

soup = BeautifulSoup(fileIn.read(), 'html.parser')

newsItems = soup.find_all('div', class_='newsItem')

for newsItem in newsItems:
    title = newsItem.find('div', class_='newsTitle').string
    content = newsItem.find('div', class_='newsContent')
    strdate = newsItem.find('div', class_='newsDate').string
    date = dateutil.parser.parse(strdate)
    fileOut = open("fundanemt_" + title.replace(" ", "-")
                                       .replace("mirrors.dotsrc.org", "dotsrc")
                                       .replace("/", "-slash-")
                                       .replace(":", "")
                                       .replace("?", "")
                                       .lower() + ".md", "w")
    fileOut.write("+++\n" +
                  f"""title = "{title}"\n""" +
                  f"""date = "{date.strftime('%Y-%m-%d')}"\n""" +
                  """+++\n""" +
                  f"""{content.prettify()}""")
    fileOut.close()
